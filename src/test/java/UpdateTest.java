import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

public class UpdateTest extends Update{
	
	private MessageTest message;
	
	public UpdateTest(Long id, String text) {
		message = new MessageTest(id, text);
	}
	
	@Override
	public Message getMessage() {
		
		return message;
	}

}
