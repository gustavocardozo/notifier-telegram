import org.telegram.telegrambots.meta.api.objects.Message;

public class MessageTest extends Message{
	
	Long id;
	String text;
	
	public MessageTest(Long id, String text) {
		this.id = id;
		text = text;
	}
	
	@Override
	public Long getChatId() {
		return id;
	}
	
	@Override
	public String getText() {
		return text;
	}

}
