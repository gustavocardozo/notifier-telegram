import org.junit.Test;
import org.telegram.telegrambots.meta.api.objects.Update;

import modelo.NotificationMessage;
import notificador.PharmedBot;
import utils.Configuration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;


public class PharmedBotTest {
    protected PharmedBot bot = new PharmedBot();
    
    @Test
    public void getBotUsernameTest() {
        assertEquals("PharmedBot", bot.getBotUsername());
        assertFalse(bot.getBotUsername().equals("not bot"));
    }
    
    @Test(expected = NullPointerException.class)
    public void onUpdateReceivedTest1() {
    	bot.onUpdateReceived(null);
    }
    
    @Test(expected = NullPointerException.class)
    public void onUpdateReceivedTest2() {
    	Update update = new Update();
    	assertNull(update.getMessage());
    	bot.onUpdateReceived(update);
    }
    
    @Test
    public void onUpdateReceivedTest3() {
    	Update update = new UpdateTest(bot.chatId, "");
    	bot.onUpdateReceived(update);
    }
    
    @Test
    public void getResponseByCommandTest() {
    	long id = 123;
    	String isInvalideComand = "invalide comand";
    	String isValideComand = "/start";
    	String invalideResponse = "Comando inválido";
        String valideResponse = "Te has suscripto! este es tu chatId: 123";
        String responseInvalide = bot.getResponseByCommand(isInvalideComand, id);
        String responseValide = bot.getResponseByCommand(isValideComand, id);
        assertEquals(invalideResponse, responseInvalide);
        assertFalse(valideResponse.equals(responseInvalide));
        assertEquals(valideResponse, responseValide);
    }
    
    @Test
    public void notifyTest() {
    	NotificationMessage notificationMessage = new NotificationMessage("Test", String.valueOf(bot.chatId), "Test Notify", Configuration.NOTIFY_MODE_TELEGRAM);
    	bot.notify(notificationMessage);
    }
}
