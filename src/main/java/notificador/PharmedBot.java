package notificador;
import interfaces.INotification;
import interfaces.Notifier;
import interfaces.NotifierTelegram;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import utils.Utilities;

public class PharmedBot extends TelegramLongPollingBot implements NotifierTelegram {

    private String botUserName = "PharmedBot";
    private String botToken = "1197271469:AAHwv7MFopc-U7Ub55eoR7CkH4FUG6woxts";
    private final String startCommand = "/start";
    public final long chatId = 471301895;

    @Override
    public void onUpdateReceived(Update update) {
        // Esta función se invocará cuando nuestro bot reciba un mensaje

        // Se obtiene el mensaje escrito por el usuario
        final String messageTextReceived = update.getMessage().getText();
        // Se obtiene el id de chat del usuario
        final long chatId = update.getMessage().getChatId();
        final String resposeMessage = getResponseByCommand(messageTextReceived, chatId);
        // Se crea un objeto mensaje
        sendMessage(resposeMessage, chatId);
    }

    public void sendMessage(String msg, long id){
        try {
            SendMessage message = new SendMessage().setChatId(id).setText(msg);
            // Se envía el mensaje
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public String getResponseByCommand(String messageTextReceived, long chatId) {
        String invalideResponse = "Comando inválido";
        String valideResponse = "Te has suscripto! este es tu chatId: %s";
        String startResponse = "Iniciado";
        if(Utilities.validateString(messageTextReceived)){
            String commandReceived = messageTextReceived.toLowerCase();
            System.out.println("Comando recibido: " + commandReceived);
            if(commandReceived.equals(startCommand)){
                return String.format(valideResponse, String.valueOf(chatId));
            }
        }
        return invalideResponse;
    }

    @Override
    public String getBotUsername() {
        return botUserName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void notify(INotification notification) {
        sendMessage(notification.getMessage(), chatId);
    }
}
